package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter  extends RecyclerView.Adapter<SuiviAdapter.ViewHolder>/* TODO Q6.a */ {
    public SuiviViewModel model;



    // TODO Q6.a
    public SuiviAdapter(SuiviViewModel model){
        this.model = model;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;

        public void setQuestions(String question){
            ((TextView) itemView.findViewById(R.id.rvQuestions)).setText(question);
        }

        public TextView getQuestionView(){return itemView.findViewById(R.id.rvQuestions); }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    @NonNull
    @Override
    public SuiviAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuiviAdapter.ViewHolder holder, int position) {
        String question = model.getQuestions(position);

        holder.getQuestionView().setText(question);
    }

    @Override
    public int getItemCount() {
        return 5;
    }
    // TODO Q7
}
