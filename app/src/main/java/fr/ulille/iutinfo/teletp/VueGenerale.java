package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.lang.reflect.Array;

public class VueGenerale extends Fragment {

    // TODO Q1
    String salle;
    String poste;
    String DISTANCIEL;

    // TODO Q2.c
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;

        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter adSalle = ArrayAdapter.createFromResource(this.getContext(), R.array.list_salles,android.R.layout.simple_spinner_item);
        spSalle.setAdapter(adSalle) ;

        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter adPoste = ArrayAdapter.createFromResource(this.getContext(), R.array.list_postes,android.R.layout.simple_spinner_item);
        spPoste.setAdapter(adPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText etLogin = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(etLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b

        update();

        AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getId()) {
                    case R.id.spSalle:

                        salle = spSalle.getSelectedItem().toString();
                        break;
                    case R.id.spPoste:
                        poste = spPoste.getSelectedItem().toString();
                        break;
                }

                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        spPoste.setOnItemSelectedListener(spinnerListener);
        spSalle.setOnItemSelectedListener(spinnerListener);

        // TODO Q9
    }

    // TODO Q5.a
    public void update(){

        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);

        if(salle.equals(DISTANCIEL)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(salle + ":" + poste);
        }
    }

    // TODO Q9
}